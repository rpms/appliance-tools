From bc03c7227fcfb4f5ebe05de2c31705c0eb94b6e7 Mon Sep 17 00:00:00 2001
From: Pablo Greco <pgreco@centosproject.org>
Date: Wed, 24 Mar 2021 10:30:46 -0300
Subject: [PATCH] Ignore grub2-install errors if they are expected

With the inclusion of
https://github.com/rhboot/grub2/commit/a1be2d182af27afb6db0e7fcf9d5ebf40086b2f6
grub2-install refuses to run in efi environments, making
appliance-create fail erroneously. The approach here is to skip this
error if expected, while still calling grub2-install to try to keep the
functionality as close as possible with older versions

Signed-off-by: Pablo Greco <pgreco@centosproject.org>
---
 appcreate/appliance.py | 12 ++++++++++--
 1 file changed, 10 insertions(+), 2 deletions(-)

diff --git a/appcreate/appliance.py b/appcreate/appliance.py
index 31de48a..474a3b9 100644
--- a/appcreate/appliance.py
+++ b/appcreate/appliance.py
@@ -74,6 +74,7 @@ class ApplianceImageCreator(ImageCreator):
         # This determines which partition layout we'll be using
         self.bootloader = None
         self.grub2inst_params = []
+        self.grub2inst_may_fail = False
 
     def _get_fstab(self):
         f = ""
@@ -201,12 +202,16 @@ class ApplianceImageCreator(ImageCreator):
                 if 'grub2-efi-arm' in packages:
                     self.bootloader = 'grub2'
                     self.grub2inst_params = ["--target=arm-efi", "--removable"]
+                    self.grub2inst_may_fail = True
                 elif 'grub2-efi-aa64' in packages:
                     self.bootloader = 'grub2'
+                    self.grub2inst_may_fail = True
                 elif 'grub2-efi-ia32' in packages:
                     self.bootloader = 'grub2'
+                    self.grub2inst_may_fail = True
                 elif 'grub2-efi-x64' in packages:
                     self.bootloader = 'grub2'
+                    self.grub2inst_may_fail = True
                 elif 'grub2' in packages or 'grub2-pc' in packages:
                     self.bootloader = 'grub2'
                 elif 'grub' in packages:
@@ -505,8 +510,11 @@ class ApplianceImageCreator(ImageCreator):
         rc = subprocess.call(["chroot", self._instroot, "grub2-install", "--no-floppy", "--no-nvram", "--grub-mkdevicemap=/boot/grub2/device.map"] + self.grub2inst_params + [loopdev])
 
         if rc != 0:
-            subprocess.call(["umount", self._instroot + "/dev"])
-            raise MountError("Unable to install grub2 bootloader")
+            if self.grub2inst_may_fail:
+                logging.debug("grub2-install failed, but error is expected and ignored (EFI)")
+            else:
+                subprocess.call(["umount", self._instroot + "/dev"])
+                raise MountError("Unable to install grub2 bootloader")
 
         mkconfig_dest = "/boot/grub2/grub.cfg"
         try:
-- 
2.31.1

